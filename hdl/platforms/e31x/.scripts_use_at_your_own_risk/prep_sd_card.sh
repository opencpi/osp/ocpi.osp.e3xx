# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License along
# with this program. If not, see <http://www.gnu.org/licenses/>.
[ -z $1 ] && echo "arg1 must specify a disk, e.g. /dev/sdb" && exit 1
echo "INFO: please ensure arg1 ($1) is the disk, NOT the partition, e.g. /dev/sdb, NOT /dev/sdb1"
FF=sdimage-gnuradio-demo.direct
wget https://files.ettus.com/e3xx_images/e3xx-release-4/ettus-e3xx-sg3/$FF.xz && \
xz -d $FF && \
sudo dd if=sdimage-gnuradio-demo.direct of=$1
