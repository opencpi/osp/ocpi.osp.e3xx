.. e3xx OSP top level project documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.

.. _e3xx-top-level:

.. |trade| unicode:: U+2122
   :ltrim:

.. |reg| unicode:: U+00AE
   :ltrim:


E3XX OSP
========
OpenCPI System support package (OSP) that contains assets that
support the Ettus Research\ |trade| Universal Software Radio Peripheral
(USRP\ |trade|) series of devices.

.. toctree::
   :maxdepth: 2

   hdl/platforms/e31x/doc/index.rst

.. will need to add hdl/cards/ and an index directory file.

